package com.example.domain.usecases

import com.example.domain.dtos.responses.ExampleResponseDto
import com.github.michaelbull.result.Result

interface IDownloadListUseCase {
    suspend fun downloadListItems(): Result<List<ExampleResponseDto>, Nothing?>
}