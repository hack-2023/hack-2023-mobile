package com.example.domain.datasources

import com.example.domain.dtos.requests.ExampleRequestDto
import com.example.domain.dtos.responses.ExampleResponseDto
import com.github.michaelbull.result.Result

interface IExampleDataSource {
    suspend fun downloadListItems(exampleRequestDto: ExampleRequestDto): Result<List<ExampleResponseDto>, Nothing?>
}