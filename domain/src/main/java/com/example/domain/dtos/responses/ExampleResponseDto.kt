package com.example.domain.dtos.responses

import kotlinx.serialization.Serializable

@Serializable
data class ExampleResponseDto(
    val id: Int,
    val title: String,
    val body: String,
    val userId: Int
)
