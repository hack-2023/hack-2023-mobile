package com.example.domain.dtos.requests

import kotlinx.serialization.Serializable

@Serializable
data class ExampleRequestDto(
    val limit: Int,
    val start: Int
)