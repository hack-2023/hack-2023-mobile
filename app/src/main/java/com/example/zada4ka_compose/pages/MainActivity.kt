package com.example.zada4ka_compose.pages

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import com.example.zada4ka_compose.components.topappbar.CustomTopAppBar
import com.example.zada4ka_compose.presentation_resources.consts.SharedPreferencesConsts

class MainActivity : AppCompatActivity() {

    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPreferences = getSharedPreferences(
            SharedPreferencesConsts.NameSharedPreferences,
            MODE_PRIVATE
        )

        setContent {
            var title: MutableState<String> = remember { mutableStateOf("") }
            val snackbarHostState = remember { SnackbarHostState() }
            val actions: MutableState<@Composable RowScope.() -> Unit> = remember { mutableStateOf({}) }
            val isTopBarIsVisible: MutableState<Boolean> = remember { mutableStateOf(false) }

            val navController = rememberNavController()

            Scaffold(
                topBar = {
                    if (isTopBarIsVisible.value) {
                        CustomTopAppBar(
                            title = { Text(text = title.value) },
                            actions = actions.value
                        )
                    } else { }
                },
                snackbarHost = { SnackbarHost(snackbarHostState) }
            ) { scaffoldPadding ->
                Box(
                    modifier = Modifier.padding(scaffoldPadding)
                ) {
                    App(
                        sharedPreferences = sharedPreferences,
                        title = title,
                        snackbarHostState = snackbarHostState,
                        navController = navController,
                        actions = actions,
                        isTopBarIsVisible = isTopBarIsVisible
                    )
                }
            }
        }
    }
}