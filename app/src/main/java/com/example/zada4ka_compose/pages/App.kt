package com.example.zada4ka_compose.pages

import android.content.SharedPreferences
import androidx.compose.foundation.layout.RowScope
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.composable
import com.example.zada4ka_compose.components.base.states.ComposablePageWithStatesBase
import com.example.zada4ka_compose.components.base.topBarAndNavigation.CustomNavigationHost
import com.example.zada4ka_compose.pages.first.FirstPage
import com.example.zada4ka_compose.pages.first.FirstPageViewModel
import com.example.zada4ka_compose.pages.mainMenu.MainMenuPage
import com.example.zada4ka_compose.pages.mainMenu.MainMenuViewModel
import com.example.zada4ka_compose.pages.second.SecondPage
import com.example.zada4ka_compose.pages.second.SecondViewModel
import com.example.zada4ka_compose.presentation_resources.consts.Routes

@Composable
fun App(
    sharedPreferences: SharedPreferences,
    title: MutableState<String>,
    snackbarHostState: SnackbarHostState,
    navController: NavHostController,
    actions: MutableState<@Composable RowScope.() -> Unit>,
    isTopBarIsVisible: MutableState<Boolean>
) {

    CustomNavigationHost(
        sharedPreferences = sharedPreferences,
        navController = navController,
        title = title,
        actions = actions,
        isTopBarIsVisible = isTopBarIsVisible
    ) {
        composable(Routes.LoginPage.name) {
            ComposablePageWithStatesBase(
                viewModelBase = (viewModel() as FirstPageViewModel),
            ) {
                FirstPage(
                    navController = navController,
                    sharedPreferences = sharedPreferences,
                )
            }
        }
        composable(Routes.LoadingListPage.name) {
            ComposablePageWithStatesBase(
                viewModelBase = (viewModel() as SecondViewModel),
                snackbarHostState = snackbarHostState
            ) {
                SecondPage()
            }
        }
        composable(Routes.MainMenu.name) {
            ComposablePageWithStatesBase(
                viewModelBase = (viewModel() as MainMenuViewModel),
                snackbarHostState = snackbarHostState
            ) {
                MainMenuPage(
                    navController = navController
                )
            }
        }
    }
}