package com.example.zada4ka_compose.pages.second

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.zada4ka_compose.pages.second.ui.SecondScreen

@Composable
fun SecondPage(
    viewModel: SecondViewModel = viewModel(),
) {
    SecondScreen(
        listItems = viewModel.listItems.collectAsState().value,
        listLoading = viewModel.listLoading.collectAsState().value,
        onItemDelete = { itemIndex -> viewModel.deleteItem(itemIndex) },
    )
}