package com.example.zada4ka_compose.pages.second.ui

import android.annotation.SuppressLint
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.example.domain.dtos.responses.ExampleResponseDto
import com.example.zada4ka_compose.pages.second.ui.components.list.ListOfItems
import com.example.zada4ka_compose.pages.second.ui.components.topbar.SecondScreenTopBar
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.receiveAsFlow

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun SecondScreen(
    listItems: List<ExampleResponseDto>,
    listLoading: Boolean,
    onItemDelete: (index: Int) -> Unit,
) {


    ListOfItems(
        listItems = listItems,
        onItemDelete = onItemDelete
    )
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        AnimatedVisibility(
            visible = listLoading,
            content = {
                CircularProgressIndicator()
            }
        )
    }

}