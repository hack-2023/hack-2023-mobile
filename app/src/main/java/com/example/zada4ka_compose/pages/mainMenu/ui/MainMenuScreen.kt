package com.example.zada4ka_compose.pages.mainMenu.ui

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.zada4ka_compose.components.interact.CustomButton

@Composable
fun MainMenuScreen(
    clickedOnButton: (Int) -> Unit,
    goToLoadingPage: () -> Unit,
    onExit: () -> Unit,
) {
    LazyColumn(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth().padding(all = 10.dp)
    ) {

        item {
            CustomButton(
                isFullWidth = true,
                onClick = goToLoadingPage
            ) {
                Text(text = "Страница с загрузкой")
            }
        }
        item {
            CustomButton(
                isDanger = true,
                onClick = {
                    clickedOnButton(2)
                }
            ) {
                Text(text = "Button 2")
            }
        }
        item {
            CustomButton(
                onClick = {
                    clickedOnButton(3)
                }
            ) {
                Text(text = "Button 3")
            }
        }
        item {
            CustomButton(
                onClick = { onExit() }
            ) {
                Text(text = "Выход")
            }
        }
    }

}