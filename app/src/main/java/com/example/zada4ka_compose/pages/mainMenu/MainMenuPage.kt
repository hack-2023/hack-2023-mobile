package com.example.zada4ka_compose.pages.mainMenu

import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.zada4ka_compose.pages.mainMenu.ui.MainMenuScreen
import com.example.zada4ka_compose.presentation_resources.consts.Routes

@Composable
fun MainMenuPage(
    navController: NavHostController,
    mainMenuViewModel: MainMenuViewModel = viewModel()
) {

    fun goToLoadingPage() {
        navController.navigate(Routes.LoadingListPage.name)
    }

    fun onExit() {
        navController.navigate(Routes.LoginPage.name) {
            this.popUpTo(0)
        }
    }

    fun clickedOnButton(buttonNumber: Int) {
        mainMenuViewModel.snackbarMessage.postValue("Clicked on $buttonNumber")
    }

    MainMenuScreen(
        clickedOnButton = ::clickedOnButton,
        goToLoadingPage = ::goToLoadingPage,
        onExit = ::onExit
    )

}