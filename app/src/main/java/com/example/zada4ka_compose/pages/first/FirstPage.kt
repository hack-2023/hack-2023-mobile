package com.example.zada4ka_compose.pages.first

import android.content.SharedPreferences
import androidx.compose.runtime.Composable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.example.zada4ka_compose.pages.first.ui.FirstScreen
import com.example.zada4ka_compose.presentation_resources.consts.Routes
import com.example.zada4ka_compose.presentation_resources.consts.SharedPreferencesConsts.Companion.LoginedSharedPreferences

@Composable
fun FirstPage(
    navController: NavHostController,
    viewModel: FirstPageViewModel = viewModel(),
    sharedPreferences: SharedPreferences,
) {

    FirstScreen(
        onEnterClick = {
            viewModel.loadSomething {
                if (viewModel.loginState) {
                    navController.navigate(Routes.MainMenu.name) {
                        popUpTo(0)
                    }
                    sharedPreferences.edit().putBoolean(LoginedSharedPreferences, true).apply()
                }
            }
        }
    )

}