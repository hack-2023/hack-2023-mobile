package com.example.zada4ka_compose.pages.first.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.zada4ka_compose.components.inputs.CustomInput
import com.example.zada4ka_compose.components.interact.CustomButton

@Composable
fun FirstScreen(onEnterClick: () -> Unit) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        CustomInput(
            value = "",
            onValueChange = {},
            modifier = Modifier.padding(bottom = 16.dp)
        )
        CustomInput(
            value = "",
            onValueChange = {},
            modifier = Modifier.padding(bottom = 8.dp)
        )
        CustomButton(
            onClick = {
                onEnterClick()
            }
        ) {
            Text(text = "Войти")
        }
    }
}