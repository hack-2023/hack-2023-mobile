package com.example.zada4ka_compose.pages.second

import androidx.lifecycle.viewModelScope
import com.example.data.usecases.DownloadListUseCase
import com.example.domain.dtos.responses.ExampleResponseDto
import com.example.zada4ka_compose.components.base.viewModel.ViewModelBase
import com.example.zada4ka_compose.presentation_resources.enums.ViewStateEnum
import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class SecondViewModel : ViewModelBase() {

    private val _downloadListUseCase = DownloadListUseCase()

    private val _listItems = MutableStateFlow(mutableListOf<ExampleResponseDto>())
    val listItems = _listItems.asStateFlow()

    private val _listLoading = MutableStateFlow(false)
    val listLoading = _listLoading.asStateFlow()

    init {
        loadListItems()
    }

    fun loadListItems() {
        viewModelScope.launch {
            state.value = ViewStateEnum.Loading

            when (val itemsResponse = _downloadListUseCase.downloadListItems()) {
                is Err -> {
                    TODO()
                }
                is Ok -> {
                    _listItems.value = itemsResponse.value.toMutableList()
                    snackbarMessage.value = "Downloaded"
                }
            }

            state.value = ViewStateEnum.Normal
        }
    }

    fun deleteItem(itemIndex: Int) {
        _listItems.value =
            _listItems.value.subList(0, itemIndex).plus(_listItems.value.subList(itemIndex + 1, _listItems.value.size))
                .toMutableList()
    }

}