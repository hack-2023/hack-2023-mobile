package com.example.zada4ka_compose.pages.second.ui.components.topbar

import android.widget.Toast
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import com.example.zada4ka_compose.components.topappbar.CustomTopAppBar

@Composable
fun SecondScreenTopBar(onExit: () -> Unit) {
    var dropDownMenuIsOpen by remember { mutableStateOf(false) }
    val context = LocalContext.current
    CustomTopAppBar(title = { Text(text = "second page bar") },
        actions = {
            IconButton(onClick = {
                dropDownMenuIsOpen = !dropDownMenuIsOpen
            }) {
                Icon(Icons.Default.MoreVert, "")
            }
            DropdownMenu(
                expanded = dropDownMenuIsOpen,
                onDismissRequest = {
                    dropDownMenuIsOpen = false
                },
            ) {
                DropdownMenuItem(
                    text = { Text("first option") },
                    onClick = {
                        Toast.makeText(context, "Clicked first option", Toast.LENGTH_SHORT).show()
                    }
                )
                DropdownMenuItem(
                    text = { Text("Выйти") },
                    onClick = onExit
                )
            }
        }
    )
}