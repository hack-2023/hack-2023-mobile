package com.example.zada4ka_compose.pages.second.ui.components.list

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.example.domain.dtos.responses.ExampleResponseDto

@Composable
fun ListOfItems(
    onItemDelete: (index: Int) -> Unit,
    listItems: List<ExampleResponseDto>,
) {
    LazyColumn(
        modifier = Modifier.fillMaxSize(),
        contentPadding = PaddingValues(16.dp),
        verticalArrangement = Arrangement.spacedBy(8.dp),
    ) {
        items(count = listItems.size) { itemIndex ->
            Card(
                modifier = Modifier.fillParentMaxWidth().defaultMinSize(minHeight = 50.dp),
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.fillMaxWidth().padding(10.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        modifier = Modifier.weight(1f),
                        text = "${listItems.get(itemIndex).id} ${listItems.get(itemIndex).title}"
                    )
                    IconButton(
                        modifier = Modifier.width(45.dp),
                        onClick = { onItemDelete(itemIndex) },
                        content = {
                            Icon(Icons.Default.Delete, "")
                        }
                    )
                }
            }
        }
    }
}