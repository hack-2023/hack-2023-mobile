package com.example.zada4ka_compose.pages.first

import androidx.lifecycle.viewModelScope
import com.example.zada4ka_compose.components.base.viewModel.ViewModelBase
import com.example.zada4ka_compose.presentation_resources.enums.ViewStateEnum
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class FirstPageViewModel : ViewModelBase() {

    var loginState = false

    fun loadSomething(callback: () -> Unit) {
        viewModelScope.launch {
            state.value = ViewStateEnum.Loading

            delay(2000)
            loginState = true

            callback.invoke()
            state.value = ViewStateEnum.Normal
        }
    }

}