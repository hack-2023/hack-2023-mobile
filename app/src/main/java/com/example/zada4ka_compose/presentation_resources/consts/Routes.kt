package com.example.zada4ka_compose.presentation_resources.consts

enum class Routes {
    LoginPage,
    LoadingListPage,
    MainMenu,
}