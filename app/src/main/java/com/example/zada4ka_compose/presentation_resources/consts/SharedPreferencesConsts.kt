package com.example.zada4ka_compose.presentation_resources.consts

open class SharedPreferencesConsts {

    companion object {
        val NameSharedPreferences = "Zada4kaSP"
        val LoginedSharedPreferences = "IsLogined"
    }

}