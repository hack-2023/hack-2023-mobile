package com.example.zada4ka_compose.presentation_resources.enums

enum class ViewStateEnum {
    Normal,
    Loading
}