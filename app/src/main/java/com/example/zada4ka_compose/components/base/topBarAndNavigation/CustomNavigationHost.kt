package com.example.zada4ka_compose.components.base.topBarAndNavigation

import android.content.SharedPreferences
import androidx.compose.foundation.layout.RowScope
import androidx.compose.runtime.*
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.example.zada4ka_compose.presentation_resources.consts.Routes
import com.example.zada4ka_compose.presentation_resources.consts.SharedPreferencesConsts

@Composable
fun CustomNavigationHost(
    sharedPreferences: SharedPreferences,
    navController: NavHostController,
    title: MutableState<String>,
    actions: MutableState<@Composable RowScope.() -> Unit>,
    isTopBarIsVisible: MutableState<Boolean>,
    builder: NavGraphBuilder.() -> Unit
) {

    NavHost(
        navController = navController,
        startDestination = if (sharedPreferences.getBoolean(
                SharedPreferencesConsts.LoginedSharedPreferences,
                false
            )
        ) Routes.MainMenu.name else Routes.LoginPage.name,
        builder = builder
    )
    TopBarSettings(
        sharedPreferences = sharedPreferences,
        navController = navController,
        title = title,
        actions = actions,
        isTopBarIsVisible = isTopBarIsVisible
    )

}