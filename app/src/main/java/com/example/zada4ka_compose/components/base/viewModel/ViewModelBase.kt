package com.example.zada4ka_compose.components.base.viewModel

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.zada4ka_compose.presentation_resources.enums.ViewStateEnum

open class ViewModelBase : ViewModel() {

    val state = mutableStateOf(ViewStateEnum.Normal)

    val snackbarMessage = MutableLiveData<String>()

}