package com.example.zada4ka_compose.components.base.states

import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import com.example.zada4ka_compose.components.base.viewModel.ViewModelBase
import com.example.zada4ka_compose.components.loading.CircularLoading
import com.example.zada4ka_compose.presentation_resources.enums.ViewStateEnum
import kotlinx.coroutines.launch

@SuppressLint(
    "StateFlowValueCalledInComposition",
    "UnusedMaterial3ScaffoldPaddingParameter",
    "CoroutineCreationDuringComposition"
)
@Composable
fun ComposablePageWithStatesBase(
    viewModelBase: ViewModelBase,
    snackbarHostState: SnackbarHostState? = null,
    children: @Composable () -> Unit
) {
    val uiState by viewModelBase.state
    val composableScope = rememberCoroutineScope()
    val snackbarMessage: String? by viewModelBase.snackbarMessage.observeAsState()

    when (uiState) {
        ViewStateEnum.Normal -> children()

        ViewStateEnum.Loading -> CircularLoading()
    }

    snackbarMessage?.let {
        Log.d("GOT MESSAGE", it)
        composableScope.launch {
            snackbarHostState?.currentSnackbarData?.dismiss()
            snackbarHostState?.showSnackbar(message = it)
        }
    }

}