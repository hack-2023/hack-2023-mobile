package com.example.zada4ka_compose.components.base.topBarAndNavigation

import android.content.SharedPreferences
import android.widget.Toast
import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavHostController
import com.example.zada4ka_compose.presentation_resources.consts.Routes
import com.example.zada4ka_compose.presentation_resources.consts.SharedPreferencesConsts

@Composable
fun TopBarSettings(
    sharedPreferences: SharedPreferences,
    navController: NavHostController,
    title: MutableState<String>,
    actions: MutableState<@Composable RowScope.() -> Unit>,
    isTopBarIsVisible: MutableState<Boolean>,
) {
    val localContext = LocalContext.current

    LaunchedEffect(navController) {
        navController.currentBackStackEntryFlow.collect { backStackEntry ->
            when (backStackEntry.destination.route) {
                Routes.LoginPage.name -> {
                    title.value = "Firstttttt"
                    actions.value = {}
                    isTopBarIsVisible.value = false
                }

                Routes.LoadingListPage.name -> {
                    isTopBarIsVisible.value = true
                    title.value = "Secondddd"
                    actions.value = {

                        var dropDownMenuIsOpen by remember { mutableStateOf(false) }

                        IconButton(onClick = {
                            dropDownMenuIsOpen = !dropDownMenuIsOpen
                        }) {
                            Icon(Icons.Default.MoreVert, "")
                        }
                        DropdownMenu(
                            expanded = dropDownMenuIsOpen,
                            onDismissRequest = {
                                dropDownMenuIsOpen = false
                            },
                        ) {
                            DropdownMenuItem(
                                text = { Text("first option") },
                                onClick = {
                                    Toast.makeText(
                                        localContext,
                                        "Clicked first option",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            )
                            DropdownMenuItem(
                                text = { Text("Выйти") },
                                onClick = {
                                    navController.navigate(Routes.LoginPage.name) {
                                        popUpTo(0)
                                    }
                                    sharedPreferences.edit()
                                        .putBoolean(SharedPreferencesConsts.LoginedSharedPreferences, false)
                                        .apply()
                                }
                            )
                        }
                    }
                }

                else -> {
                    title.value = ""
                    isTopBarIsVisible.value = false
                }
            }
        }
    }
}