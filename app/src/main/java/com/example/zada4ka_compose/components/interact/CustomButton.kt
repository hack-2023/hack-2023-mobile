package com.example.zada4ka_compose.components.interact

import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color

@Composable
fun CustomButton(
    isFullWidth: Boolean = false,
    isFullHeight: Boolean = false,
    isDanger: Boolean = false,
    onClick: (() -> Unit),
    content: @Composable RowScope.() -> Unit
) {
    var modifier = Modifier
        .then(Modifier)

    if (isFullWidth)
        modifier = modifier
            .then(Modifier.fillMaxWidth())
    if (isFullHeight)
        modifier = modifier.then(Modifier.fillMaxHeight())

    Button(
        colors = ButtonDefaults.buttonColors(containerColor = if (isDanger) Color.Red else Color.Blue),
        modifier = modifier,
        onClick = onClick,
        content = content
    )
}