package com.example.data.datasources

import com.example.core.httpClient.KtorClient
import com.example.domain.datasources.IExampleDataSource
import com.example.domain.dtos.requests.ExampleRequestDto
import com.example.domain.dtos.responses.ExampleResponseDto
import kotlinx.coroutines.delay

class ExampleDataSource : IExampleDataSource {
    override suspend fun downloadListItems(exampleRequestDto: ExampleRequestDto) =
        KtorClient.get<List<ExampleResponseDto>>("posts")
}