package com.example.data.usecases

import com.example.data.datasources.ExampleDataSource
import com.example.domain.dtos.requests.ExampleRequestDto
import com.example.domain.dtos.responses.ExampleResponseDto
import com.example.domain.usecases.IDownloadListUseCase
import kotlinx.coroutines.delay

class DownloadListUseCase : IDownloadListUseCase {

    private val _exampleDataSource = ExampleDataSource()

    override suspend fun downloadListItems() = run {
        val request = ExampleRequestDto(
            limit = 0,
            start = 0
        )
        _exampleDataSource.downloadListItems(exampleRequestDto = request)
    }
}