package com.example.core.httpClient

import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.github.michaelbull.result.Err
import com.github.michaelbull.result.Ok
import com.github.michaelbull.result.Result
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.android.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.util.*
import kotlinx.coroutines.time.delay
import java.time.Duration

class KtorClient {

    companion object {
        @Volatile
        private var ktorClientInstance: HttpClient? = null


        fun getInstance(): HttpClient {
            if (ktorClientInstance == null) {
                synchronized(this) {
                    if (ktorClientInstance == null) {
                        ktorClientInstance = HttpClient(Android) {
                            install(Logging) {
                                level = LogLevel.BODY
                                logger = object : Logger {
                                    override fun log(message: String) {
                                        Log.d("KTOR CLIENT", message)
                                    }

                                }
                            }
                            defaultRequest {
                                url {
                                    protocol = URLProtocol.HTTPS
                                    host = "jsonplaceholder.typicode.com"
                                }
                            }
                            install(ContentNegotiation) {
                                json()
                            }
                        }
                    }
                }
            }
            return ktorClientInstance!!
        }

        suspend inline fun <reified T> get(url: String) =
            getCorrectResponse<T>(getInstance().get(url))

        @OptIn(InternalAPI::class)
        suspend inline fun <reified T> getWithBody(url: String, requestBody: Any) =
            getCorrectResponse<T>(getInstance().get(url) {
                body = requestBody
            })

        suspend inline fun <reified T> post(url: String) = getCorrectResponse<T>(getInstance().post(url))

        @OptIn(InternalAPI::class)
        suspend inline fun <reified T> postWithBody(url: String, requestBody: Any) =
            getCorrectResponse<T>(getInstance().post(url) {
                body = requestBody
            })

        @RequiresApi(Build.VERSION_CODES.O)
        suspend inline fun <reified T> getCorrectResponse(response: HttpResponse): Result<T, Nothing?> {
            // this delay is for imitation of loading
            delay(Duration.ofSeconds(2))
            return if (response.status.isSuccess()) {
                Ok(value = response.body<T>())
            } else {
                Err(error = null)
            }
        }
    }
}